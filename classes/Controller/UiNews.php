<?php defined('SYSPATH') or die('No direct script access.');

class Controller_UiNews extends Controller_Admin_Access  {

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        //Recupera o objeto do e acordo com o id informado
        $ui_news_language = ORM::factory('UiNewsLanguage')
            ->with('ui_news')
            ->with('ui_language')
            ->where('ui_language_id', '=', '1')
            ->find_all();

        //Recupera o objeto do e acordo com o id informado
        $ui_language = ORM::factory('UiLanguage')->find_all();

        //Renderiza a view
        $this->template->content = View::factory('UiNews/Index')
            ->set('ui_news_language', $ui_news_language)
            ->set('ui_language', $ui_language);
    }

    public function action_add()
    {
        $this->action_form();
    }

    public function action_edit()
    {
        $this->action_form();
    }

    public function action_form()
    {
        //Define o array de erros
        $errors = array();

        //Recupera o id informado, caso não exista retorna 0
        $ui_news_id = $this->request->param('id', 0);

        //Recupera o id informado, caso não exista retorna 0
        $ui_language_id = $this->request->param('parent_id', '1');

        //Recupera o objeto do e acordo com o id informado
        $ui_news = ORM::factory('UiNews', $ui_news_id);

        //Recupera o objeto do e acordo com o id informado
        $ui_news_language = ORM::factory('UiNewsLanguage')
                           ->where('ui_news_id', '=', $ui_news_id)
                           ->where('ui_language_id', '=', $ui_language_id)
                           ->find();

        //Recupera o objeto do e acordo com o id informado
        $ui_language = ORM::factory('UiLanguage')->where('id', '=', $ui_language_id)->find();

        //Recebe os dados que vieram via POST
        $params = $this->request->post();

        //Verifica se recebeu algum dado
        if ($params)
        {
            //Realiza a validação dos dados
            $validate = Validation::factory($params);
            $validate->rule('title','not_empty');

            if ($validate->check()){

                if(!empty($params['active'])) $params['active'] = '1'; else $params['active'] = '0';
                if(!empty($params['date'])) $params['date'] = Ui_Date::convert_us($params['date']);

                $ui_news->values($params);
                $ui_news->save();

                $ui_news_language->values($params);
                $ui_news_language->ui_news_id = $ui_news->id;

                if(!empty($_FILES['image']['name']))
                {
                    //Verifica se a imagem anterior existe então apaga
                    if(!empty($ui_news_language->image))
                    {
                        $image_original = DOCROOT .'uploads/news/'. $ui_news->id .'/'. $ui_news_language->image;
                        $image_large    = DOCROOT .'uploads/news/'. $ui_news->id .'/large/'. $ui_news_language->image;
                        $image_small    = DOCROOT .'uploads/news/'. $ui_news->id .'/small/'. $ui_news_language->image;

                        if(file_exists($image_original)) @unlink($image_original);
                        if(file_exists($image_large)) @unlink($image_large);
                        if(file_exists($image_small)) @unlink($image_small);
                    }

                    $ui_news_language->image = md5(uniqid(time())) .".". pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                }

                if($ui_news_language->save())
                {
                    if(!empty($_FILES['image']['name']))
                        $ui_news_language->upload('image', $ui_news->id, $ui_news_language->image);

                    $this->redirect($this->module_link);
                }
            } else {
                //Recupera os erros
                $errors = $validate->errors('register');
            }
        }

        //Renderiza a view
        $this->template->content = View::factory('UiNews/Form')
            ->set('errors', $errors)
            ->set('ui_language', $ui_language)
            ->set('ui_news_language', $ui_news_language);
    }

    public function action_remove()
    {
        //Recupera o id informado, caso não exista retorna 0
        $id = $this->request->param('id', 0);

        //Recupera o objeto do e acordo com o id informado
        $ui_news = ORM::factory('UiNews', $id);

        //Recupera o objeto do e acordo com o id informado
        $ui_news_language = ORM::factory('UiNewsLanguage')
                            ->where('ui_news_id', '=', $ui_news->id)
                            ->where('ui_language_id', '=', '1')
                            ->find();

        //Recebe os dados que vieram via POST
        $params = $this->request->post();

        //Verifica se recebeu algum dado
        if ($params)
        {
            if(Ui_File::remove_dir(DOCROOT .'uploads/news/'. $ui_news->id))
            {
                $ui_news_language = ORM::factory('UiNewsLanguage')
                    ->where('ui_news_id', '=', $ui_news->id)
                    ->find_all();

                foreach($ui_news_language as $item_ui_news_language)
                {
                    //Apaga o conteúdo
                    $item_ui_news_language->delete();
                }

                if($ui_news->delete())
                    $this->redirect($this->module_link);
            }
        }

        //Renderiza a view
        $this->template->content = View::factory('UiNews/Remove')
             ->set('ui_news_language', $ui_news_language);
    }
}