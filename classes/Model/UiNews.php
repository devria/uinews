<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Raphael Azeredo
 */
class Model_UiNews extends ORM {

    protected $_table_name      = 'ui_news';
    protected $_sorting         = array('id' => 'ASC');

    protected $_has_many        = array(
        'ui_news_language'      => array()
    );
}