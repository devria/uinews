<?php defined('SYSPATH') or die('No direct script access.'); ?>

<div class="dvButtonsTop">
    <div class="group-right">
        <?= Ui_Link::add($ui_module_access, $module_link .'/add'); ?>
    </div>
</div>
<br />
<div class="widget-box">
    <div class="widget-title">
        <h5><?= $module_title ?></h5>
    </div>
    <div class="widget-content nopadding">
        <table id="newsDataTables" class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Ativo</th>
                    <th>Editar</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($ui_news_language as $item_ui_news_language): ?>
                    <tr>
                        <td><?= $item_ui_news_language->ui_news->id ?></td>
                        <td><?= $item_ui_news_language->title ?></td>
                        <td><?php if($item_ui_news_language->active) echo 'Sim'; else echo 'Não'; ?></td>
                        <td>
                            <div class="btn-group">
                                <?php foreach($ui_language as $item_ui_language): ?>
                                    <?= Ui_Link::edit_language($ui_module_access, $module_link.'/edit/'. $item_ui_news_language->ui_news->id .'/'. $item_ui_language->id, $item_ui_language->icon); ?>
                                <?php endforeach; ?>
                            </div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <?= Ui_Link::remove($ui_module_access, $module_link.'/remove/'. $item_ui_news_language->ui_news->id); ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#newsDataTables').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aoColumns": [
                { 	"bSortable": true       ,
                    "sClass": "center",
                    "sWidth": "50"
                },
                { 	"bSortable": true,
                    "sClass": "left"
                },
                { 	"bSortable": true,
                    "sClass": "center",
                    "sWidth": "60"
                },
                { 	"bSortable": false,
                    "sClass": "center",
                    "sWidth": "150"
                },
                { 	"bSortable": false,
                    "sClass": "center",
                    "sWidth": "100"
                }
            ]
        });
        $('select').select2();
    } );
</script>