<?php defined('SYSPATH') or die('No direct script access.'); ?>

<section id="forms">
    <div class="row">
        <div class="span10 offset1">
            <form id="formNews" method="post" class="form-horizontal well">
                <fieldset>
                    <legend>Gerenciador de notícias</legend>
                    <div class="control-group">
                        <label class="control-label"><strong>Idioma</strong></label>
                        <div class="control-remove">
                            <?php if(!empty($ui_news_language->ui_language->icon)): ?>
                                <img src="<?= url::site() .'uploads/icons/'. $ui_news_language->ui_language->icon ?>">
                            <?php endif; ?>
                            <?= $ui_news_language->ui_language->name ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><strong>Data</strong></label>
                        <div class="control-remove">
                            <?= Date::formatted_time($ui_news_language->date, 'd/m/Y') ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><strong>Título</strong></label>
                        <div class="control-remove">
                            <?= $ui_news_language->title ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><strong>Descrição</strong></label>
                        <div class="control-remove">
                            <?= $ui_news_language->description ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><strong>Conteúdo</strong></label>
                        <div class="control-remove">
                            <?= $ui_news_language->content ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><strong>Imagem</strong></label>
                        <div class="control-remove">
                            <?php if(!empty($ui_news_language->image)): ?>
                            <img src="<?= url::site() .'uploads/news/'. $ui_news_language->ui_news_id .'/small/'. $ui_news_language->image?>">
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><strong>Ativo</strong></label>
                        <div class="control-remove">
                            <?php if($ui_news_language->active == '1') echo 'Sim'; else echo 'Não'; ?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <p>Tem certeza que deseja remover esse registro?</p>
                        <?= Form::submit('btRemove', 'Sim', array('class' => 'btn btn-success')) ?>
                        <?= HTML::anchor($module_link, '<button type="button" class="btn btn-danger">Não</button>'); ?>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</section>